#include <iostream>
#include <boost/function.hpp>

class TracerFunctor
{
public:
	TracerFunctor()
	{
		std::cout << "TracerFunctor::TracerFunctor()" << std::endl;
	}

	TracerFunctor(const TracerFunctor& t)
	{
		std::cout << "TracerFunctor::TracerFunctor(const TracerFunctor& t)" << std::endl;
	}

	TracerFunctor& operator=(const TracerFunctor& t)
	{
		std::cout << "Tracer::operator=(const Tracer& t)" << std::endl;
		return *this;
	}

	~TracerFunctor()
	{
		std::cout << "Tracer::~Tracer()" << std::endl;
	}

	void operator()(const std::string& s, int count) const
	{
		for(int i = 0; i < count; ++i)
			std::cout << s << "\n";
	}
};

int main()
{

	std::cout << "Utworzenie obiektu funkcyjnego:\n";

	// przez watrość
	{
		TracerFunctor tf;

		std::cout << "Przypisanie do obiektu typu function:\n";

		boost::function<void (std::string, int)> f = tf;

        std::cout << "Wywolanie poprzez f:\n";

        f("Przez wartosc", 5);
	}

	std::cout << "-----------------------------------------\n";

	// przez referencję
	{
		TracerFunctor tf;

		std::cout << "Przypisanie do obiektu typu function:\n";

		boost::function<void (std::string, int)> f = boost::ref(tf);

        std::cout << "Wywolanie poprzez f:\n";

        f("Przez wartosc", 5);
	}
}
