#include <cstdlib>
#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <boost/shared_ptr.hpp>

using namespace std;


struct FileCloser
{
	void operator()(FILE* file) 
	{
        std::cout << "Wywołanie FileCloser uchwytu FILE* - plik zostanie zamkniety" << std::endl;
		if ( file != 0 )
			fclose(file);
	}
};


int main()
try
{
	std::cout << "-- shared_ptr z własnym dealokatorem\n";

	FILE* f = fopen("./text.txt", "w");
	if ( f == 0 )
	{
		std::cout << "Nie mozna otworzyc pliku" << std::endl;
		throw std::runtime_error("Nie mozna otworzyc pliku");
	}

	{ // blok 1
		boost::shared_ptr<FILE> my_shared_file(f, FileCloser());

		std::cout << "Zapis do pliku z bloku 1\n";
		fprintf(my_shared_file.get(), "Test shared_ptr<FILE>\n");

		{ // blok 2
			std::cout << "Zapis do pliku z bloku 2\n";
			fprintf(my_shared_file.get(), "Test shared_ptr<FILE>\n");
			std::cout << "Koniec bloku 2\n";
		}
		std::cout << "Zapis do pliku z bloku 1\n";
		fprintf(my_shared_file.get(), "Test shared_ptr<FILE>\n");
		std::cout << "Koniec bloku 1\n";
	}
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;
}
