#include <memory>
#include <iostream>

using namespace std;

class X
{
public:
	// konstruktor
	X(int value = 0)
		: value_(value)
	{
		std::cout << "Konstruktor X(" << value_ << ")\n"; 
	}
	
	// destruktor
	~X() 
	{
		std::cout << "Destruktor ~X(" << value_ << ")\n"; 
	}
	
	int& value()
	{
		return value_;
	}

	const int& value() const
	{
		return value_;
	}
private:
	int value_;
};

void unsafe1()  // TODO: poprawa z wykorzystaniem auto_ptr
{
	X* ptrX = new X(4);

	/* kod korzystajacy z ptrX */

	throw int(13);
	/* jeszcze troche kodu */
	
	delete ptrX;
}

// TODO: Napisac funkcje typu source & sink

// TODO: Napisac klase ze skladowa auto_ptr<X>

int main()
{
	try 
	{
		unsafe1();
	}
	catch(...)
	{
		std::cout << "Zlapalem wyjatek!" << std::endl;
	}
}