#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <memory>
#include <boost/scoped_ptr.hpp>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjatki #1
********************************************************************/

class Device
{
private:
	size_t devno_;
public:
	Device(int devno) : devno_(devno)
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Konstruktor Device #" << devno << endl;
	}

    size_t devno() const
    {
        return devno_;
    }

	~Device()
	{
		cout << "Destruktor Device #" << devno_ << endl;
	}
};

class Broker 
{
public:
    Broker(int devno1, int devno2) : dev1_(new Device(devno1)), dev2_(new Device(devno2))
	{
    }

    Broker(const Broker& source) : dev1_(new Device(source.dev1_->devno())), dev2_(new Device(source.dev2_->devno()))
    {
    }

	~Broker()
	{
	}
private:
    boost::scoped_ptr<Device> dev1_;
    boost::scoped_ptr<Device> dev2_;
};

int main()
{
	try
	{
		Broker b(1, 2);

        Broker b2 = b;
    }
	catch(const exception& e)
	{
		cerr << "Wyjatek: " << e.what() << endl;
	}
}
