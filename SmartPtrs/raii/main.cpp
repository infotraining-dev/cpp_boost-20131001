#include <iostream>
#include <stdexcept>

using namespace std;

namespace ThreadingAPI
{
    class Mutex
    {
    public:
        void lock()
        {
            cout << "lock()" << endl;
        }

        void unlock()
        {
            cout << "unlock()" << endl;
        }
    };
}

class LockGuard
{
    ThreadingAPI::Mutex& mtx_;
    LockGuard(const LockGuard&);
    LockGuard& operator=(const LockGuard&);
public:
    LockGuard(ThreadingAPI::Mutex& mtx) : mtx_(mtx)
    {
        mtx_.lock();
    }

    ~LockGuard()
    {
        mtx_.unlock();
    }
};

void foo()
{
    throw std::runtime_error("Error");
}

void do_sth()
{
    ThreadingAPI::Mutex mtx;

    LockGuard lk(mtx);

    cout << "do_sth()\n";
    foo();
}

int main()
{
    try
    {
        do_sth();
    }
    catch(exception& ex)
    {
        cout << ex.what() << endl;
    }
}

