#include <iostream>
#include <string>
#include <boost/tuple/tuple.hpp>

using namespace std;

class Base
{
public:
	virtual ~Base() {}
	virtual string test() const
	{
		return "Base::test()";
	}
};

class Derived : public Base
{
public:
	virtual string test() const
	{
		return "Derived::test()";
	}
};

boost::tuple<int, string, string> get_record()
{
    return boost::make_tuple(13, "Jan", "Kowalski");
}

template <typename T, int N>
struct TuplePrinter
{
    static void print_element(T t)
    {
        cout << t.get<N>() << " ";
        TuplePrinter<T, N-1>::print_element(t);
    }
};

template <typename T>
struct TuplePrinter<T, 0>
{
    static void print_element(T t)
    {
        cout << t.get<0>() << endl;
    }
};

template <typename T>
void print_tuple(T t)
{
    TuplePrinter<T, boost::tuples::length<T>::value-1>::print_element(t);
}

int main()
{
	boost::tuple<int, string, Derived> t1(-5, "Krotka1");
	boost::tuple<unsigned int, string, Base> t2;

	t2 = t1;

	cout << "t2.get<0>() = " << t2.get<0>() << "\n"
		 << "t2.get<1>() = " << t2.get<1>() << "\n"
		 << "t2.get<2>().test() = " << t2.get<2>().test() << "\n";

    int id;
    string name, lastname;

    boost::tie(id, name, lastname) = get_record();

    cout << "id: " << id << " name: " << name << " lname: " << lastname << endl;

    boost::tuple<int, string, double> t3 = boost::make_tuple(1, "One", 3.14);

    print_tuple(t3);
}
