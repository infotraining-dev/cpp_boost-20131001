#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <boost/any.hpp>
#include <boost/variant.hpp>
#include <boost/foreach.hpp>

using namespace std;

class IsEmpty : public unary_function<boost::any, bool>
{
public:
    bool operator()(const boost::any& a)
    {
        return a.empty();
    }
};

template <typename T>
class IsType : public unary_function<boost::any, bool>
{
public:
    bool operator()(const boost::any& a)
    {
        return typeid(T) == a.type();
    }
};

class PrintingVisitor : public boost::static_visitor<>
{
public:
    void operator()(int& x)
    {
        cout << "int: " << x << endl;
    }

    void operator()(string& s)
    {
        cout << "string: " << s << endl;
    }
};


int main()
{
	vector<boost::any> store_anything;

    store_anything.push_back(1);
	store_anything.push_back(5);
	store_anything.push_back(string("three"));
	store_anything.push_back(3);
	store_anything.push_back(string("four"));
	store_anything.push_back(string("one"));
	store_anything.push_back(string("eight"));
	store_anything.push_back(5);
	store_anything.push_back(4);
	store_anything.push_back(boost::any());
	store_anything.push_back(string("five"));
	store_anything.push_back(string("six"));
	store_anything.push_back(boost::any());


	/* TO DO :
     * Wykorzystując algorytmy biblioteki standardowej wykonaj nastapujące czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
     * 1 - przefiltruj wartosci niepuste w kolekcji stored_anything
	 * 2 - zlicz ilosc elementow typu int oraz typu string
	 * 3 - wyekstraktuj z kontenera store_anything do innego kontenera wszystkie elementy typu string
	 */

	// 1
    store_anything.erase(remove_if(store_anything.begin(), store_anything.end(), IsEmpty()), store_anything.end());
	cout << "store_anything.size() = " << store_anything.size() << endl;

	// 2
	int count_int;
	// TODO
    count_int = count_if(store_anything.begin(), store_anything.end(), IsType<int>());
    cout << "stored_anything przechowuje " << count_int << " elementow typu int" << endl;


	int count_string;
	// TODO
    count_string = count_if(store_anything.begin(), store_anything.end(), IsType<string>());
    cout << "stored_anything przechowuje " << count_string << " elementow typu string" << endl;

    // TODO
    typedef boost::variant<string, int> VariantStrInt;
    vector<VariantStrInt> store_variants;

    BOOST_FOREACH(boost::any a, store_anything)
    {
        if (a.type() == typeid(int))
            store_variants.push_back(boost::any_cast<int>(a));

        if (a.type() == typeid(string))
            store_variants.push_back(boost::any_cast<string>(a));
    }

    cout << "sizeof string == " << sizeof(string) << endl;
    cout << "sizeof Variant<string, int> == " << sizeof(VariantStrInt) << endl;

    cout << "Variants: ";

    BOOST_FOREACH(VariantStrInt v, store_variants)
    {
        cout << v << " ";
    }
    cout << endl;

    cout << "Only strings: ";

    BOOST_FOREACH(VariantStrInt v, store_variants)
    {
        if (string* pstr = boost::get<string>(&v))
            cout << *pstr << " ";
    }
    cout << endl;

    // użycie wizytatora
    cout << "Wizytacja:\n";

    PrintingVisitor printer;
    for_each(store_variants.begin(), store_variants.end(), boost::apply_visitor(printer));
    cout << "\n";

    // 3

	list<string> string_items;

    string (*cast)(const boost::any&) = &boost::any_cast<string>;

    transform(store_anything.begin(), partition(store_anything.begin(), store_anything.end(), IsType<string>()),
              back_inserter(string_items), cast);

	cout << "string_items: ";
	copy(string_items.begin(), string_items.end(),
			ostream_iterator<string>(cout, " "));
	cout << endl;
}
