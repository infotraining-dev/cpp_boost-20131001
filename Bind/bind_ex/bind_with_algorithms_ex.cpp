#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <numeric>
#include <boost/bind.hpp>

using namespace std;

int main()
{
	vector<Person> employees;
	fill_person_container(employees);

	cout << "Wszyscy pracownicy:\n";
	copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
	cout << endl;

	// wyswietl pracownikow z pensja powyzej 3000
	cout << "\nPracownicy z pensja powy�ej 3000:\n";
//    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
//            boost::bind(greater<double>(), boost::bind(&Person::salary, _1), 3000.0));

    double threshold = 3000.0;

    boost::function<bool (const Person&)> check_salary = [=](const Person& p) { return p.salary() > threshold; };

    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            check_salary);

    // wy�wietl pracownikow o wieku ponizej 30 lat
    cout << "\nPracownicy o wieku ponizej 30 lat:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            boost::bind(less<int>(), boost::bind(&Person::age, _1), 30));
	
	// posortuj malej�co pracownikow wg nazwiska
    cout << "\nLista pracownikow wg nazwiska (malejaco):\n";
    sort(employees.begin(), employees.end(),
         boost::bind(greater<string>(),
                        boost::bind(&Person::name, _1),
                        boost::bind(&Person::name, _2)));

    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;
	
    // wyswietl kobiety
	cout << "\nKobiety:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            boost::bind(equal_to<Gender>(), boost::bind(&Person::gender, _1), Female));

    double sum = accumulate(employees.begin(), employees.end(), 0,
                         boost::bind(std::plus<double>(), boost::bind(&Person::salary, _2), _1));

    double avg = sum / employees.size();

    cout << "Avg salary: " << avg << endl;
}
