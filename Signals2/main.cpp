#include <iostream>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>

using namespace std;
using namespace boost;

void print()
{
    cout << "Hello\n";
}

class Printer
{
public:
    void operator()()
    {
        cout << "Hello from functor\n";
    }
};

class Watcher
{
    int counter_;
public:
    Watcher() : counter_(0)
    {
    }

    void count()
    {
        counter_++;
    }

    int counter() const
    {
        return counter_;
    }
};

class TempMonitor
{
    signals2::signal<void (double)> temp_changed_;
public:
    void register_callback(boost::function<void(double)> callback)
    {
        temp_changed_.connect(callback);
    }

    void run()
    {
        for(double t = 10.5; t < 30; ++t)
            temp_changed_(t);
    }
};

void print_temp(double t)
{
    cout << "Biezaca temperatura: " << t << endl;
}

int main()
{
    signals2::signal<void ()> sig;

    sig.connect(&print);

    Printer prn;
    sig.connect(prn);

    Watcher watcher;
    sig.connect(bind(&Watcher::count, &watcher));

    // wywolanie sygnalu
    for(int i = 0; i < 10; ++i)
            sig();

    cout << "Counter: " << watcher.counter() << endl;

    TempMonitor monitor;

    monitor.register_callback(&print_temp);

    monitor.run();
}
